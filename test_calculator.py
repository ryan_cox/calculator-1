import unittest
import calculator as cal
from platform import system

class TestMathsMethods(unittest.TestCase):
    def test_add_positive(self):
        self.assertEqual(cal.add(cal.dec.Decimal('1'), cal.dec.Decimal('3')), cal.dec.Decimal('4'))
    
    def test_add_negative(self):
        self.assertEqual(cal.add(cal.dec.Decimal('1'), cal.dec.Decimal('-3')), cal.dec.Decimal('-2'))
    
    def test_add_decimal(self):
        self.assertEqual(cal.add(cal.dec.Decimal('0.45'), cal.dec.Decimal('2.99')), cal.dec.Decimal('3.44'))
    
    def test_subtract_positive(self):
        self.assertEqual(cal.subtract(cal.dec.Decimal('4'), cal.dec.Decimal('3')), cal.dec.Decimal('1'))
    
    def test_subtract_negative(self):
        self.assertEqual(cal.subtract(cal.dec.Decimal('1'), cal.dec.Decimal('-3')), cal.dec.Decimal('4'))
    
    def test_subtract_decimal(self):
        self.assertEqual(cal.subtract(cal.dec.Decimal('0.45'), cal.dec.Decimal('2.99')), cal.dec.Decimal('-2.54'))
    
    def test_multiply_positive(self):
        self.assertEqual(cal.multiply(cal.dec.Decimal('2'), cal.dec.Decimal('3')), cal.dec.Decimal('6'))
    
    def test_multiply_negative(self):
        self.assertEqual(cal.multiply(cal.dec.Decimal('2'), cal.dec.Decimal('-3')), cal.dec.Decimal('-6'))
    
    def test_multiply_decimal(self):
        self.assertEqual(cal.multiply(cal.dec.Decimal('0.45'), cal.dec.Decimal('2.99')), cal.dec.Decimal('1.3455'))
    
    def test_divide_positive(self):
        self.assertEqual(cal.divide(cal.dec.Decimal('6'), cal.dec.Decimal('3')), cal.dec.Decimal('2'))
    
    def test_divide_negative(self):
        self.assertEqual(cal.divide(cal.dec.Decimal('-6'), cal.dec.Decimal('3')), cal.dec.Decimal('-2'))
    
    def test_divide_decimal(self):
        self.assertEqual(cal.divide(cal.dec.Decimal('3'), cal.dec.Decimal('1.5')), cal.dec.Decimal('2'))

@unittest.skipIf(system() == 'Linux', 'Running on Linux') # Used to prevent execution of GUI based tests on BitBucket Pipeline.
class TestGUIMethods(unittest.TestCase):
    def setUp(self):
        self.btnFrm = cal.ButtonFrame(cal.tk.Tk())
        self.button_character_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '-', '*', '/', '.']
        self.char = self.button_character_list[0]
    
    def tearDown(self):
        self.btnFrm.destroy()
    
    def test_button_text(self):
        btns = self.btnFrm.generate_buttons(self.button_character_list)
        self.assertEqual(btns[0]['text'], self.char)

class TestValidationMethods(unittest.TestCase):
    # Test that the entry of operands works
    def test_validation_first_term(self):
        self.assertTrue(cal.validate('4', ''))
    
    def test_validation_second_term(self):
        self.assertTrue(cal.validate('4', '3+4'))    
    
    # Test that the entry of operators work
    def test_validation_operator_add(self):
        self.assertTrue(cal.validate('+', '5+'))
        
    def test_validation_operator_subtract(self):
        self.assertTrue(cal.validate('-', '5-'))
        
    def test_validation_operator_multiply(self):
        self.assertTrue(cal.validate('*', '5*'))
        
    def test_validation_operator_divide(self):
        self.assertTrue(cal.validate('/', '5/'))
    
    # Test that two operators can't be entered
    def test_validation_operator_double(self):
        self.assertFalse(cal.validate('/', '2+/'))
    
    # But equals can
    def test_validation_operator_equals('=', '4+3='))
        
        
if __name__ == '__main__':
    unittest.main()