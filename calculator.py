# Import modules
import decimal as dec
import tkinter as tk
# Wildcard (*) imports of tkinter are used in much of the teaching material
# but can cause problems with namespace conflicts.

value_char_list = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '0', '.']
cmd_char_list = ['+', '-', '*', '/', '=', 'Clr']
mode = ''

def add(a, b):
    """Add two numbers."""
    return a + b

def subtract(a, b):
    """Subtract two numbers."""
    return a - b

def multiply(a, b):
    """Multiply two numbers."""
    return a * b

def divide(a, b):
    """Divide two numbers."""
    return a / b

def validate(char, string):
    """Validates input.
    
    Input must be in value_char_list or cmd_char_list.
    """
    if not char in (value_char_list + cmd_char_list) and not char == '':
        return False
    return True

def solve(gui, *args):
    """Process input and carries out commands.
    
    If the input contains = it returns the solution and resets style.
    Elif input contains Clr or is empty it clears everything and resets style.
    Elif input contains an operator it styles it.
    
    :param gui: The Interface object.
    :param *args: tkinter includes arguments with the callback that we need to
    accept.
    """
    string = gui.input_str.get()
    # Handle clearing and cleared strings
    if 'Clr' in string or gui.input_str == '':
        gui.input_str.set('')
        gui.buttonsFrm.unmark()
        return
    # Solve input
    elif '=' in string:
        string = string.strip('=')
        mode = ''
        for char in cmd_char_list:
            if char in string:
                mode = char
                break
        term1, term2 = string.split(mode)
        # Convert to Decimal object for accuracy with floating point numbers.
        term1 = dec.Decimal(term1)
        term2 = dec.Decimal(term2)
        if mode == '+':
            solution = add(term1, term2)
        elif mode == '-':
            solution = subtract(term1, term2)
        elif mode == '*':
            solution = multiply(term1, term2)
        elif mode == '/':
            solution = divide(term1, term2)    
        gui.input_str.set(string + '=' + str(solution))
        gui.buttonsFrm.unmark()
        return
    # Mark selected operand
    else:
        for char in string:
            if char in cmd_char_list:
                gui.buttonsFrm.mark(char)
                return


class Interface:
    
    """Creates and manages the GUI components.
    
    :var input_str: The current input from buttons and/or the textbox.
    :method __init__: Class constructor
    """    
    
    def __init__(self, parent):
        """Create GUI frames and initalise interface wide variables."""
        self.parent = parent
        self.input_str = tk.StringVar()        
        self.textboxFrm = TextboxFrame(self)
        self.buttonsFrm = ButtonFrame(self)
        # Call solve whenever input_str changed
        self.input_str.trace_add('write', lambda *args: solve(self))
    

class TextboxFrame(tk.Frame):
    
    """Frame that includes textbox for keyboard entry and input display.
    
    :var parent: Parent tk object known as gui
    :var textbox: tk.Entry object
    :method __init__: Class constructor
    :method on_entry: Character entry handling
    """
    
    def __init__(self, parent):
        """Construct TextboxFrame"""
        super(TextboxFrame, self).__init__()
        self.parent = parent
        #self.grid(row = 0)
        self.pack()
        on_entry = self.register(self.on_entry)
        self.textbox = tk.Entry(self, textvariable = self.parent.input_str,
                                validate = 'key',
                                validatecommand = (on_entry, '%S', '%P'))
        self.textbox.pack()
    
    def on_entry(self, char, string):
        """Calls input validation and adds input to string
        
        :return True: Validated as valid and added to string.
        :return False: Validated as invalid. Prevents entry.
        """
        if not validate(char, string):
            return False
        return True
        
        
        
class ButtonFrame(tk.Frame):
    
    """Frame that includes all the buttons for user input.
    
    :var parent: Parent tk object known as gui
    :var button_character_list: All the symbols used for buttons.
    :var btns: A list of tk.Button objects
    :method __init__: Class constructor
    :method generate_buttons: Turn character list into tk.Button objects
    :method display_buttons: Display buttons from list
    :method add_character: Proccess and add selected char to input_str
    :method mark: Style relief of chosen button.
    :method unmark: Style relief of all buttons to return to inital state.
    """
    
    def __init__(self, parent):
        """Class constructor"""
        super(ButtonFrame, self).__init__()
        self.parent = parent
        #self.grid(row = 1)
        self.pack()
        self.button_character_list = value_char_list + cmd_char_list
        self.btns = self.generate_buttons(self.button_character_list)
        self.display_buttons(self.btns, 3, 6)
    
    def generate_buttons(self, btn_list):
        """Take list of characters (as strings) and turn them into tk.Button
        objects."""
        btns = []
        for char in btn_list:
            btns.append(tk.Button(self, text = char, command = lambda char=char:
                                  self.add_character(char), height = 1,
                                  width = 3, bg = '#ccc' if char in value_char_list else '#fff'))
        return btns
    
    def display_buttons(self, btns, width, height):
        """Arrange tk.Button objects from a list into a grid."""
        index = 0
        for rowvar in range(height):
            for columnvar in range(width):
                btns[index].grid(row = rowvar, column = columnvar)
                index += 1
                if index == len(btns):
                    break
    
    def add_character(self, char):
        """Add a character to the current input"""
        if validate(char, self.parent.input_str):
            self.parent.input_str.set(self.parent.input_str.get() + char)
        
    def mark(self, char):
        """Change button styling to indicate selection."""
        for button in self.btns:
            if char == button['text']:
                button['relief'] = 'sunken'
                break
    
    def unmark(self):
        """Style all buttons as per default, reversing mark()"""
        for button in self.btns:
            button['relief'] = 'raised'
        

if __name__ == '__main__':
    """Run GUI."""
    root = tk.Tk()
    gui = Interface(root)
    root.title('Calculator')
    #root.minsize(100, 200)
    root.mainloop()